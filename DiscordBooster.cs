﻿using DiscordBooster.Config;
using DiscordBooster.DiscordApi;
using DiscordBooster.EcoLinker;
using DiscordBooster.ThreadHandler;
using Eco.Core.Plugins.Interfaces;
using EcoColorLib;
using System;
using System.Threading;

namespace DiscordBooster
{

    public class DiscordBooster : IModKitPlugin, IServerPlugin
    {

        public static String prefix = ChatFormat.Green.Value + ChatFormat.Bold.Value + "DiscordBooster: " + ChatFormat.Clear.Value;

        private static Boolean initialized = false;

        public static Boolean disableOnlineMessage = false;


        public string GetStatus()
        {
            if(!initialized)
            {
                initialized = true;
                initialize();
            }
   

            return "";
        }

        public static void initialize()
        {
            reloadConfig();

            Thread thread = new Thread(() => DiscordBot.startBot());
            thread.Start();

            RewardThread.startThread();

            EventWatch.registredListener();


           

        }

         

        public static void reloadConfig()
        {
            ConfigDiscordBooster.instance = new ConfigDiscordBooster("DiscordBooster", "baseConfig");
            if (ConfigDiscordBooster.instance.exist())
            {
                ConfigDiscordBooster.instance = ConfigDiscordBooster.instance.reload<ConfigDiscordBooster>();
            }
            else
            {
                ConfigDiscordBooster.instance.save();
            }


            ConfigDiscordUsername.instance = new ConfigDiscordUsername("DiscordBooster", "usernameConfig");
            if (ConfigDiscordUsername.instance.exist())
            {
                ConfigDiscordUsername.instance = ConfigDiscordUsername.instance.reload<ConfigDiscordUsername>();
            }
            else
            {
                ConfigDiscordUsername.instance.save();
            }



            LanguageDiscordBooster.instance = new LanguageDiscordBooster("DiscordBooster", "languageConfig");
            if (LanguageDiscordBooster.instance.exist())
            {
                LanguageDiscordBooster.instance = LanguageDiscordBooster.instance.reload<LanguageDiscordBooster>();
            }
            else
            {
                LanguageDiscordBooster.instance.save();
            }
    
          
        }
    }
}
