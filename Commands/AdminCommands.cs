﻿using DiscordBooster.Config;
using DiscordBooster.DiscordApi;
using Eco.Gameplay.Economy;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Chat;
using EcoColorLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBooster.Commands
{

    public enum SubCommands
    {
        testMsg
    }

    public class AdminCommands : IChatCommandHandler
    {
		/*
        [ChatCommand("treasurygive", "Give 50 cheated money Eco to treasury", ChatAuthorizationLevel.Admin)]
        public static void TreasurygiveCommand(User user, String subCommand = "")
        {
            Currency currency = EconomyManager.Currency.GetCurrency("Eco");
            Account a = currency.GetAccount(AccountNames.TreasuryName);
            a.SetVal(a.Val+50);
            user.Player.SendTemporaryMessage("Gived 50 Eco");
        }*/

        [ChatCommand("dba", "DiscordBooster Admin commands", ChatAuthorizationLevel.Admin)]
        public static void DiscordBoosterAdminCommand(User user,String subCommand ="")
        {
            if(subCommand==null || subCommand.Equals(""))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix+LanguageDiscordBooster.instance.AdminSubCommand));

                foreach(SubCommands sub in Enum.GetValues(typeof(SubCommands)))
                {
                    user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + "-"+ChatFormat.Red.Value+sub.ToString()));
                } 
            }
            else
            {
                SubCommands sub;
   
                if(!SubCommands.TryParse(subCommand, out sub))
                {
                    user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + String.Format(LanguageDiscordBooster.instance.NoSubCommand, subCommand)));
                }
                else
                {
                   
                    if(sub.Equals(SubCommands.testMsg))
                    {
                        user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + "Trying to send a message to discord chanel"));
                        DiscordBot.instance.sendMessageToChannel("Yo yo ");
                    }
                    else
                    {
                        user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + String.Format(LanguageDiscordBooster.instance.NoSenseError, sub.ToString())));
                    }
                }

            }

        }

    }
}
