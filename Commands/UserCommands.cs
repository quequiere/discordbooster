﻿using Discord.WebSocket;
using DiscordBooster.Config;
using DiscordBooster.DiscordApi;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Chat;
using EcoColorLib;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace DiscordBooster.Commands
{

    public enum SubCommandsUser
    {
        link
    }

    public class UserCommands : IChatCommandHandler
    {

        public static Dictionary<string, string> tempLink = new Dictionary<string, string>();


        // /db link pseudo
        [ChatCommand("db", "DiscordBooster user commands", ChatAuthorizationLevel.User)]
        public static void DiscordBoosterUserCommand(User user, String argsString = "")
        {
            String[] args = argsString.Split(' ');


            if (args.Length<=0 || args[0].Equals(""))
            {
                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + LanguageDiscordBooster.instance.AdminSubCommand));

                foreach (SubCommandsUser sub in Enum.GetValues(typeof(SubCommandsUser)))
                {
                    user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + "/db " + ChatFormat.Red.Value + sub.ToString()));
                }
                return;
            }
            else
            {
                SubCommandsUser sub;

                if (!SubCommandsUser.TryParse(args[0], out sub))
                {
                    user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + String.Format(LanguageDiscordBooster.instance.NoSubCommand, args[0])));
                }
                else
                {

                    if (sub.Equals(SubCommandsUser.link))
                    {
                        if (args.Length <= 1 || args[1].Equals(""))
                        {
                            user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + String.Format(LanguageDiscordBooster.instance.UserSubCommandAddUsernameError)));
                        }
                        else
                        {


                            if(ConfigDiscordUsername.instance.ecoNameRegistred(user.Name))
                            {
                                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + String.Format(LanguageDiscordBooster.instance.UserSubCOmmandAlreadyLinked)));
                                return;
                            }


                            SocketGuildUser discordUser =  DiscordBot.instance.getUserByName(args[1]);

                            if (discordUser == null)
                            {
                                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + String.Format(LanguageDiscordBooster.instance.UserSubCommandNotOnlineError, args[1])));
                                return;
                            }
                            else
                            {

                                String messageToSend = String.Format(LanguageDiscordBooster.instance.DiscordMessageLinker, args[1],user.Name);
                                DiscordBot.instance.SendMessageToUser(discordUser, messageToSend);
                                user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + String.Format(LanguageDiscordBooster.instance.UserSubCommandSendKey)));
                                tempLink.Add(args[1], user.Name);
                                return;
                            }
                         
                        }

                    }
                    else
                    {
                        user.Player.SendTemporaryMessage(FormattableStringFactory.Create(DiscordBooster.prefix + String.Format(LanguageDiscordBooster.instance.NoSenseError, sub.ToString())));
                    }
                }

            }
        }

    }
}
