﻿using Discord;
using Discord.Net;
using Discord.WebSocket;
using DiscordBooster.Commands;
using DiscordBooster.Config;
using DiscordBooster.ThreadHandler.DiscordWatcher;
using Eco.Gameplay.Systems.Chat;
using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DiscordBooster.DiscordApi
{
    public class DiscordBot
    {
        public static DiscordBot instance;

        public static Boolean antiRebootBot = false;

        private readonly DiscordSocketClient _client;

        public static void startBot()
        {
            Console.WriteLine("Trying to start Discord bot");
            instance = new DiscordBot();
            instance.MainAsync().GetAwaiter().GetResult();
        }

        private DiscordBot()
        {
            _client = new DiscordSocketClient(new DiscordSocketConfig { LogLevel = LogSeverity.Error, });
        }

        private static Task Logger(LogMessage message)
        {
            var cc = Console.ForegroundColor;
            switch (message.Severity)
            {
                case LogSeverity.Critical:
                case LogSeverity.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case LogSeverity.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case LogSeverity.Info:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case LogSeverity.Verbose:
                case LogSeverity.Debug:
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    break;
            }
            Console.WriteLine($"{DateTime.Now,-19} [{message.Severity,8}] {message.Source}: {message.Message}");
            Console.ForegroundColor = cc;
            return Task.CompletedTask;
        }

        private async Task MainAsync()
        {
            if(antiRebootBot)
            {
                Console.WriteLine("Trying relaunch ? This is an error, plz send message to dev");
                return;
            }

            _client.Log += Logger;
            _client.LoginAsync(TokenType.Bot, ConfigDiscordBooster.instance.botSecretToken);
            await _client.StartAsync();

            await InitCommands();

            Console.WriteLine("Discord bot should be online");


            antiRebootBot = true;
            await Task.Delay(-1);

        }

        private async Task InitCommands()
        {
             _client.MessageReceived += HandleCommandAsync;
        }

        private async Task HandleCommandAsync(SocketMessage arg)
        {

            var msg = arg as SocketUserMessage;
            if (msg == null) return;

            if(msg.Author.IsBot)
            {
                return;
            }


            //si c'est un message prive
           if (msg.Channel.GetType() == typeof(SocketDMChannel))
           {
                String content = msg.Content;
                String author = msg.Author.Username;

                SocketGuildUser user = getUserByName(author);

                if (content.Equals("oklink"))
                {
                    String ecoTarget;

                    if (UserCommands.tempLink.TryGetValue(author, out ecoTarget))
                    {
                        ConfigDiscordUsername.instance.addLink(ecoTarget, author);

                        UserCommands.tempLink.Remove(author);

                        ChatManager.ServerMessageToAll(FormattableStringFactory.Create(String.Format(LanguageDiscordBooster.instance.DiscordSuccessLinkServerMessage, ecoTarget)), false);

                        DiscordWatcherEvent.onLinkedAccountEvent(user, ecoTarget);

                    }
                    else
                    {
                        SendMessageToUser(user, String.Format(LanguageDiscordBooster.instance.DiscordNoInvite));
                    }
                }
                else if(content.Equals("!bienvenue"))
                {
                    String ecoName = ConfigDiscordUsername.instance.getEcoName(author);
                    if (ecoName==null)
                    {
                        DiscordBot.instance.SendMessageToUser(user, "Un problème est survenu, avez vous lié votre compte discord et eco ?");
                        Console.WriteLine("Unregistred error player discord booster");
                    }
                    else
                    {
                        Console.WriteLine("DiscordBooster: a discord user askink for welcome gift ! "+ user.Username+" / "+ecoName);
                        DiscordWatcherEvent.onAskingMoneyWelcome(user, ecoName);
                    }

                    
                }
                else
                {
                    SendMessageToUser(user, "Hello, I'm the DiscordBooster bot provided by DiscordBooster Mod, for eco. Link: eco-serveur.fr");
                }

           }


        }

        public SocketGuild getGuild()
        {
            return _client.GetGuild(ConfigDiscordBooster.instance.discordServerId); 
        }

        public SocketGuildUser getUserByName(String name)
        {
            SocketGuild guild = getGuild();
            IEnumerator enumerator = guild.Users.GetEnumerator();
            while (enumerator.MoveNext())
            {
                SocketGuildUser user = enumerator.Current as SocketGuildUser;

                if (user.Username.Equals(name))
                {
                    return user;
                }
            }

            return null;

        }

        public void sendMessageToChannel(String message)
        {
            SocketGuild guild = getGuild();
            try
            {
                //general 417010395375730718
                //debug 426009598554669056
                //plcmarche 420321596180398112
                SocketTextChannel channel = guild.GetTextChannel(420321596180398112);
                
                if(guild==null)
                {
                    Console.WriteLine("Error can't find guild !");
                    return;
                }

                if(channel==null)
                {
                    Console.WriteLine("Error can't find channel !");
                    return;
                }

                channel.SendMessageAsync(message);
            }
            catch(Exception e)
            {
                Console.WriteLine("Error while sending message to discord channel: " + e.Message);
            }
               
        }


        public Boolean isDiscordUserOnline(String name)
        {
            SocketGuild guild = getGuild();
            IEnumerator enumerator = guild.Users.GetEnumerator();
            while (enumerator.MoveNext())
            {
                SocketGuildUser user = enumerator.Current as SocketGuildUser;

                if (user.Username.Equals(name))
                {
                   if(UserStatus.Online.Equals(user.Status))
                    {
                        return true;
                    }
                   else
                    {
                        return false;
                    }
                }
            }

            return false;

        }


        public Boolean isDiscordUserVocal(String name)
        {
            SocketGuild guild = getGuild();
            IEnumerator enumerator = guild.Users.GetEnumerator();
            while (enumerator.MoveNext())
            {
                SocketGuildUser user = enumerator.Current as SocketGuildUser;

                if (user.Username.Equals(name))
                {
                    if(user.VoiceChannel!=null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }

            return false;

        }

        public async void SendMessageToUser(SocketGuildUser discordUser,String message)
        {
            try
            {
                await discordUser.SendMessageAsync(message);
            }
            catch(HttpException e)
            {
                Console.WriteLine("Critical error in discordbooster while send message to: " + discordUser.Nickname+" ==> "+e.Message);
            }
           
        }
    }
}
