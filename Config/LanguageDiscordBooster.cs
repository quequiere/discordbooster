﻿using EcoColorLib;
using JsonConfigSaver;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBooster.Config
{
    public sealed class LanguageDiscordBooster : JsonEcoConfig
    {

        public static LanguageDiscordBooster instance;


        //general message
        [JsonProperty]
        public String AdminSubCommand = ChatFormat.Yellow.Value + "Les sous commandes disponibles sont les suivantes:" + ChatFormat.Clear.Value;

        [JsonProperty]
        public String NoSubCommand = ChatFormat.Red.Value + "Desoler mais la sous commande {0} n'existe pas." + ChatFormat.Clear.Value;

        [JsonProperty]
        public String NoSenseError = ChatFormat.Red.Value + "Cette erreur ne devrait pas arriver avec l'argument {0}" + ChatFormat.Clear.Value;


        //description message
        [JsonProperty]
        public String AdminSubCommandIgnoreDescription = ChatFormat.Yellow.Value + "Le canal discord dans lequel vous etes sera ignore de la liste des joueurs online" + ChatFormat.Clear.Value;

        [JsonProperty]
        public String UserSubCommandAddUsernameError = ChatFormat.Red.Value + "Merci d'ajouter votre pseudo discord a cette commande" + ChatFormat.Clear.Value;

        [JsonProperty]
        public String UserSubCOmmandAlreadyLinked = ChatFormat.Red.Value + "Vous etes deja link avec discord" + ChatFormat.Clear.Value;

        [JsonProperty]
        public String UserSubCommandNotOnlineError = ChatFormat.Red.Value + "Nous n'arrivons pas a trouver le compte {0} sur notre discord. L'avez vous rejoint ?" + ChatFormat.Clear.Value;

        [JsonProperty]
        public String UserSubCommandSendKey = ChatFormat.Green.Value + "Nous vous avons envoye un message prive sur Discord. Veuillez le lire." + ChatFormat.Clear.Value;

        [JsonProperty]
        public String UserSubCommandLinkSucces = ChatFormat.Green.Value + "Felicitation ! Votre discord {0} est bien lie a votre compte eco {1}" + ChatFormat.Clear.Value;

        [JsonProperty]
        public String DiscordMessageLinker = "Salut {0} ! Une demande de jonction Eco/Discord a ete faite par {1}. Si c'est bien toi repond a ce message par 'oklink'";

        [JsonProperty]
        public String DiscordSuccessLink = "Vous avez ete lie avec succes au compte eco {0}";

        [JsonProperty]
        public String DiscordSuccessLinkServerMessage = " <b><#33ccff>{0}</b>" + " a link son compte Eco et discord avec la commande /db ! </color>";

        [JsonProperty]
        public String DiscordNoInvite = "No invite, you need to use '/db link' in game first.";

        [JsonProperty]
        public String rewarderMessageNotRegistred = ChatFormat.Gray.Value + "Vous n'avez pas joint votre compte discord et eco, vous n'avez donc pas gagne de recompense. Plus d'info: /db" + ChatFormat.Clear.Value;

        [JsonProperty]
        public String rewarderMessagehalfReward = ChatFormat.Green.Value + "Vous etes en ligne sur discord mais pas dans un canal oral, nous ne gagne que {0} % xp." + ChatFormat.Clear.Value;

        [JsonProperty]
        public String rewarderMessagefullReward = ChatFormat.Green.Value + "Vous avez gagne {0} % xp car vous etes en ligne sur discord dans un canal oral !" + ChatFormat.Clear.Value;

        [JsonProperty]
        public String rewarderMessageNotOnline = ChatFormat.Gray.Value + "Vous devez etre en ligne sur le discord pour gagner un bonus d'XP" + ChatFormat.Clear.Value;

        public LanguageDiscordBooster(string plugin, string name) : base(plugin, name)
        {
            
        }

    }
}
