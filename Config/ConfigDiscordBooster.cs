﻿using JsonConfigSaver;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBooster.Config
{
    public sealed class ConfigDiscordBooster : JsonEcoConfig
    {
        public static ConfigDiscordBooster instance;
        [JsonProperty]
        public ArrayList chanelIgnoreList;

        [JsonProperty]
        public float fullOnlineExperiencePercent = 3f;

        [JsonProperty]
        public float halfOnlineExperiencePercent = 1f;

        [JsonProperty]
        public int scanTimeInMinutes = 15;

        [JsonProperty]
        public ulong discordServerId = 999999999999999999;

        [JsonProperty]
        public string botSecretToken = "qqqqqqqqqqqqqqqqqqqqqqqq.qqqqqq.qqqqqqqqqqqqqqqqqqqqqqqqqqq";



        public ConfigDiscordBooster(string plugin, string name) : base(plugin, name)
        {
            this.chanelIgnoreList=new ArrayList();
        }



    }
}
