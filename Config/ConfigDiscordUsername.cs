﻿using Eco.Gameplay.Players;
using JsonConfigSaver;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBooster.Config
{
    public sealed class ConfigDiscordUsername : JsonEcoConfig
    {

        public static ConfigDiscordUsername instance;


        //eco to discord
  
        private Dictionary<string, string> cachePlayerToDiscord;


        [JsonProperty]
        private List<KeyValuePair<string, string>> playerToDiscordSave;

        [JsonProperty]
        private ArrayList userWelcomeReceived;
  


        public ConfigDiscordUsername(string plugin, string name) : base(plugin, name)
        {
            this.cachePlayerToDiscord= new Dictionary<string, string>();
            playerToDiscordSave = new List<KeyValuePair<string, string>>();

        }

        public void addUserWelcomeReceived(String name)
        {
            if(!this.getuserWelcomeReceived().Contains(name))
            {
                this.userWelcomeReceived.Add(name);
                this.save();
            }
           
        }

        public Boolean hasReceivedWelcomeMoney(String ecoName)
        {
            if (this.getuserWelcomeReceived().Contains(ecoName))
            {
                return true;
            }

            return false;
        }

        private ArrayList getuserWelcomeReceived()
        {
            if(this.userWelcomeReceived==null)
            {
                this.userWelcomeReceived = new ArrayList();

                foreach(String name in UserManager.Usernames)
                {
                    this.userWelcomeReceived.Add(name);
                }

                Console.WriteLine("Generated " + this.userWelcomeReceived.Count + " users in welcome gift list.");

                this.save();
            }

            return this.userWelcomeReceived;
        }

        public Boolean discordNameRegistred(String discordName)
        {
            if(cachePlayerToDiscord.ContainsValue(discordName))
            {
                return true;
            }
            return false;
        }

        public void addLink(String eco,String discord)
        {

            cachePlayerToDiscord.Add(eco, discord);
            playerToDiscordSave.Add(new KeyValuePair<string, string>(eco, discord));
            this.save();
        }

        public override void postLoaded<T>(T obj)
        {
            base.postLoaded<T>(obj);

            ConfigDiscordUsername conf = obj as ConfigDiscordUsername;

            foreach (KeyValuePair<string, string> val in conf.playerToDiscordSave)
            {
                conf.cachePlayerToDiscord.Add(val.Key,val.Value);
            }

            Console.WriteLine("DiscordBooster: Players loaded:" + conf.cachePlayerToDiscord.Count());
        }


        public Boolean ecoNameRegistred(String ecoName)
        {
            if (cachePlayerToDiscord.ContainsKey(ecoName))
            {
                return true;
            }
            return false;
        }


        public String getDiscordName(String ecoName)
        {
            String name = null;
            cachePlayerToDiscord.TryGetValue(ecoName, out name );
            return name;
        }

        public String getEcoName(String discordNamme)
        {

            foreach(String key in cachePlayerToDiscord.Keys)
            {
                String value = null;
                cachePlayerToDiscord.TryGetValue(key, out value);

                if(value ==null)
                {
                    continue;
                }

                if (discordNamme.Equals(value))
                {
                    return key;
                }
            }
            return null;
        }


        }
}
