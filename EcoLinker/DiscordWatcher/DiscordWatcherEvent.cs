﻿using Discord.WebSocket;
using DiscordBooster.Config;
using DiscordBooster.DiscordApi;
using Eco.Gameplay.Economy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBooster.ThreadHandler.DiscordWatcher
{
    class DiscordWatcherEvent
    {
        public static float moneyToGiveWelcome = 50;

        public static void sendBigMessage(ArrayList message, SocketGuildUser user)
        {
            String messageFinal = "";
            foreach (String s in message)
            {
                messageFinal += s + "\n";
            }

            DiscordBot.instance.SendMessageToUser(user, messageFinal);
        }

        public static void onAskingMoneyWelcome(SocketGuildUser user, String ecoName)
        {
            ArrayList message = new ArrayList();
            message.Add("__Nous procédons à la livraison de votre argent de bienvenue__");

            String money = "Eco";
            Currency currency = EconomyManager.Currency.GetCurrency(money);

            if (currency==null)
            {
                message.Add("La monnaie "+ money+" n'existe pas sur le serveur");
                sendBigMessage(message, user);
                return;
            }

            if (AccountNames.TreasuryName==null || !currency.HasAccount(AccountNames.TreasuryName))
            {
                message.Add("Il ne semble pas exister de mairie pour le moment. Impossible de vous verser des fonds");
                sendBigMessage(message, user);
                return;
            }

            if (!currency.HasAccount(AccountNames.TreasuryName))
            {
                message.Add("Il ne semble pas exister de mairie pour le moment. Impossible de vous verser des fonds");
                sendBigMessage(message, user);
                return;
            }

            Account treasury = currency.GetAccount(AccountNames.TreasuryName);

            if(ConfigDiscordUsername.instance.hasReceivedWelcomeMoney(ecoName))
            {
                message.Add("Vous avez déjà recu cette récompense !");
                sendBigMessage(message, user);
                return;
            }
            else if(treasury.Val>= moneyToGiveWelcome)
            {
                currency.CreditAccount(AccountNames.TreasuryName, -50);
                currency.CreditAccount(ecoName, 50);
                ConfigDiscordUsername.instance.addUserWelcomeReceived(ecoName);
                message.Add("Nous vous avons livré 50 Ecos en jeu des coffres de la mairie !");
                sendBigMessage(message, user);
                return;
            }
            else
            {
                //_Treasury
                message.Add("La mairie (compte: "+ AccountNames.TreasuryName + " ) n'a plus assez de trésorierie pour vous livrer.");
                message.Add("Veuillez réésayer plus tard");
                sendBigMessage(message, user);
                return;
            }

       
        }

        public static void onLinkedAccountEvent(SocketGuildUser user,String ecoName)
        {
            //SendMessageToUser(user, String.Format(LanguageDiscordBooster.instance.DiscordSuccessLink, ecoTarget));
            ArrayList message = new ArrayList();
            message.Add("__Félicitation nous avons bien lié ton compte Discord et eco "+(ecoName)+"__");
            message.Add("");
            message.Add("Tu peux à présent réclamer tes 50 Ecos (monnaie du serveur) de bienvenue en me répondant: **!bienvenue**");

            sendBigMessage(message, user);
        }

        public static void onNewDiscordUserEvent(SocketGuildUser user)
        {
            ArrayList message = new ArrayList();
            message.Add("__**Salut à toi "+user.Username+" !**__");
            message.Add("Bienvenue sur le serveur http://eco-serveur.fr");

            message.Add("");

            message.Add("Lie ton compte discord et Eco en jeu en utilisant la commande suivante: **/db link "+ user.Username+"**");
            message.Add("En faisant cette manipulation tu recevras **50 Ecos** (monnaie du serveur) de bienvenue afin de démarrer l'aventure.");
            message.Add("Et tu gagneras entre 1,5 et 3% de skill point supplémentaire toutes les 20 minutes !");

            message.Add("");

            message.Add("Tu peux aussi utiliser la commande **/vote** en jeu afin d'obtenir des bonus supplémentaires.");

            message.Add("");

            message.Add("N'oublie pas de faire un __petit tour dans le règlement__ ;)");

            message.Add("");

            message.Add("Bon jeu à toi !");


            sendBigMessage(message, user);
        }
    }
}
