﻿using Discord.WebSocket;
using DiscordBooster.DiscordApi;
using DiscordBooster.ThreadHandler.ContractWatcher;
using DiscordBooster.ThreadHandler.DiscordWatcher;
using Eco.Gameplay.Economy;
using Eco.Gameplay.Economy.Contracts;
using Eco.Shared.Items;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DiscordBooster.EcoLinker
{
    class WatcherThread
    {
        public static int sleepTimeSeconds = 10;
        private Boolean waiterFirstTime = false;
        ArrayList contracts = new ArrayList();
        ArrayList discordUsers = new ArrayList();


        public void run()
        {
            while (Thread.CurrentThread.IsAlive)
            {
                if(!waiterFirstTime)
                {
                    Thread.Sleep(7 * 1000);
                    waiterFirstTime = true;
                    loadContracts();
                    loadDiscordUsers();
                }

                this.checkContractChange();
                this.checkNewDiscordUsers();
                Thread.Sleep(sleepTimeSeconds * 1000);
            }

            Console.WriteLine(DiscordBooster.prefix + "WatcherThread has been stoped !");
        }




        private void checkNewDiscordUsers()
        {
            SocketGuild guild = DiscordBot.instance.getGuild();
            IEnumerator enumerator = guild.Users.GetEnumerator();

            while (enumerator.MoveNext())
            {
                SocketGuildUser user = enumerator.Current as SocketGuildUser;

                if (!discordUsers.Contains(user.Id))
                {
                    discordUsers.Add(user.Id);
                    //quequiere debug
                    //DiscordWatcherEvent.onNewDiscordUserEvent(user);
                }
            }

        }

        private void loadDiscordUsers()
        {
            SocketGuild guild = DiscordBot.instance.getGuild();
            IEnumerator enumerator = guild.Users.GetEnumerator();
            while (enumerator.MoveNext())
            {
                SocketGuildUser user = enumerator.Current as SocketGuildUser;
                discordUsers.Add(user.Id);
            }

        }



        private void loadContracts()
        {
            foreach (Contract c in EconomyManager.Contracts.Contracts)
            {
                this.contracts.Add(c);
            }

            Console.WriteLine(DiscordBooster.prefix + this.contracts.Count + " contracts registred.");

            if(!DiscordBooster.disableOnlineMessage)
            {
                DiscordBot.instance.sendMessageToChannel("**Le serveur est à présent en ligne !**");
            }
            
        }

        private void checkContractChange()
        {
            foreach (Contract c in EconomyManager.Contracts.Contracts)
            {
                if (!this.contracts.Contains(c))
                {
                    if(c.State.Equals(ContractState.Active) || c.State.Equals(ContractState.Submitted) || c.State.Equals(ContractState.Unassigned))
                    {
                        this.contracts.Add(c);
                        ContractWatcherTool.onNewContractCreated(c);
                    }

                 
                }
            }
        }
    }
}
