﻿using DiscordBooster.DiscordApi;
using Eco.Gameplay.Economy.Contracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBooster.ThreadHandler.ContractWatcher
{
    class ContractWatcherTool
    {
        public static void onNewContractCreated(Contract c)
        {
            ArrayList message = new ArrayList();

            message.Add("__**Un nouveau contrat a été créé par " + c.ClientUser.Name + " :**__");
            message.Add("");

            message.Add("Nom du contrat: " + c.Name);
            message.Add("Montant à déposer: " + c.DepositAmount());
            message.Add("Montant payé: " + c.PaymentAmount());
            message.Add("");
         
            int x = 1;
            foreach (ContractClause cla in c.Clauses)
            {
                message.Add("__Clause " + x+ "__");
                message.Add("**TooltipTitle ==>** " + cla.TooltipTitle);

                message.Add("**Describe ==>** " + cla.Describe);
                
                message.Add("");
                x++;
            }

            String messageFinal = "";
            foreach(String s in message)
            {
                messageFinal += s+"\n";
            }


            DiscordBot.instance.sendMessageToChannel(messageFinal);
        }
    }
}
