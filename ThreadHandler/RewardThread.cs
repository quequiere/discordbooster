﻿using DiscordBooster.Config;
using DiscordBooster.DiscordApi;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DiscordBooster.ThreadHandler
{
    public class RewardThread
    {
        public static void startThread()
        {
            Thread thread = new Thread(() => rewarded());
            thread.Start();
        }

        private static void rewarded()
        {

            while(true)
            {

                Thread.Sleep(ConfigDiscordBooster.instance.scanTimeInMinutes*60*1000);


                foreach (User onlineU in UserManager.OnlineUsers)
                {
                    if(ConfigDiscordUsername.instance.ecoNameRegistred(onlineU.Name))
                    {
                        String discordName = ConfigDiscordUsername.instance.getDiscordName(onlineU.Name);


                        if (discordName == null)
                        {
                            ChatManager.ServerMessageToPlayer(FormattableStringFactory.Create("This is a bug #563"), onlineU, false);
                        }
                        else if (DiscordBot.instance.isDiscordUserVocal(discordName))
                        {
                            float xpToAdd = ConfigDiscordBooster.instance.fullOnlineExperiencePercent / 100f;
                            onlineU.UseXP(-xpToAdd);
                            ChatManager.ServerMessageToPlayer(FormattableStringFactory.Create(String.Format(LanguageDiscordBooster.instance.rewarderMessagefullReward, ConfigDiscordBooster.instance.fullOnlineExperiencePercent)), onlineU, false);
                        }
                        else if (DiscordBot.instance.isDiscordUserOnline(discordName))
                        {
                            float xpToAdd = ConfigDiscordBooster.instance.halfOnlineExperiencePercent / 100f;
                            onlineU.UseXP(-xpToAdd);
                            ChatManager.ServerMessageToPlayer(FormattableStringFactory.Create(String.Format(LanguageDiscordBooster.instance.rewarderMessagehalfReward, ConfigDiscordBooster.instance.halfOnlineExperiencePercent)), onlineU, false);
                        }
                        else 
                        {
                            ChatManager.ServerMessageToPlayer(FormattableStringFactory.Create(String.Format(LanguageDiscordBooster.instance.rewarderMessageNotOnline, 999)), onlineU, false);
                        }

                       }
                    else
                    {
                        ChatManager.ServerMessageToPlayer(FormattableStringFactory.Create(String.Format(LanguageDiscordBooster.instance.rewarderMessageNotRegistred)), onlineU, false);
                    }
                    
                }


            }
        }
    }
}
